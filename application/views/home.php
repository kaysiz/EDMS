<!DOCTYPE html>
<html>

<!-- Head -->
<head>

	<title>B W Kahari</title>

	<!-- Meta-Tags -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="keywords" content="Library Member Login Form Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- //Meta-Tags -->

	<!-- Style --> <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css" media="all">

	<!-- Fonts -->
		<link href="//fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
	<!-- //Fonts -->

</head>
<!-- //Head -->

<!-- Body -->
<body>

	<h1>Welcome to <br />B W Kahari | Legal Practitioners and Attorneys at Law</h1>

	<div class="container w3layouts agileits">

		<div class="login w3layouts agileits">
			<h2 style="text-align: center;">Log In</h2>
			<form action="#" method="post">
				<input type="text" Name="Userame" placeholder="Username" required="">
				<input type="password" Name="Password" placeholder="Password" required="">
			</form>
			<ul class="tick w3layouts agileits">
				<li>
					<input type="checkbox" id="brand1" value="">
					<label for="brand1"><span></span>Remember me</label>
				</li>
			</ul>
			<div class="send-button w3layouts agileits">
				<form>
					<input type="submit" value="Sign In">
				</form>
			</div>
			<a href="#">Forgot Password?</a>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>

	</div>

	<div class="footer w3layouts agileits">
		<p> &copy; 2016 All Rights Reserved</p>
	</div>

</body>
<!-- //Body -->

</html>
